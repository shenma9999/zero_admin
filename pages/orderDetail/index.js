import api from "../../common/api.js"
import interaction from "../../common/interaction"
import utils from '../../common/utils'
Page({
  data: {
    status: 0,
    navHeight: 0,
    orderInfo: {},
    remark: "",
    showRemark: false,
    updateDate: "",
    playStatus: ""
  },
  onLoad(options) {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      uid: options.order_uid
    })
    interaction.showLoading("加载中...")
    api.getOrderDetail(options.order_uid).then(res => {
      res.statusStr = utils.getStatusStr(res.status)
      // 1:其他 2:微信支付 3:余额支付 4:次卡支付
      res.payment_method_cn = utils.getPaymethodStr(res.payment_method)
      if (res.payment_method == 4) {
        res.total_amount = parseInt(res.total_amount)
      }
      if (res.extra_json.started_at && res.extra_json.ended_at) {
        this.setData({
          playStatus: "已离场"
        })
      } else if (res.category == 2 && res.extra_json.started_at && !res.extra_json.ended_at) { // 计时
        this.setData({
          playStatus: "已入场"
        })
      } else if (res.category == 2 && !res.extra_json.started_at && !res.extra_json.ended_at) { // 计时
        this.setData({
          playStatus: "未入场"
        })
      } else if (res.category == 3 && !res.extra_json.started_at && !res.extra_json.ended_at) { // 计次
        this.setData({
          playStatus: "未入场"
        })
      } else if (res.category == 3 && res.extra_json.started_at) { // 计次
        this.setData({
          playStatus: "已入场"
        })
      }
      this.setData({
        orderInfo: res,
        updateDate: utils.dateFormat("yyyy/MM/dd hh:mm:ss", res.extra_json.started_at)
      })
      interaction.hideLoading()
    }).catch(ex => {
      interaction.hideLoading()
    })
  },
  onShowRemark() {
    this.setData({
      showRemark: true
    })
  },
  onSetRemark() {
    if (!this.data.remark) {
      interaction.toast("备注不能为空!")
      return false
    }
    interaction.showLoading("处理中...")
    api.setOrderRemark(this.data.orderInfo.uid, this.data.remark).then(res => {
      interaction.hideLoading()
      interaction.toast("备注添加成功!")
      let str = "orderInfo.remark"
      this.setData({
        [str]: this.data.remark
      })
    }, err => {
      interaction.hideLoading()
    })
  },
  onCloseRemark() {
    this.setData({
      showRemark: false
    })
  },
  onGiveMoney() {
    wx.showModal({
      content: "是否确认退款？",
      confirmText: "确定",
      cancelText: "取消",
      success: function (e) {
        if (e.confirm) {
          interaction.toast("确定了")
        } else {
          // 用户点了取消
        }
      }
    })
  }
})

import interaction from "../../common/interaction"
import api from "../../common/api"
Page({
  data: {
    status: 0,
    navHeight: 0,
    username: "",
    password: "",
  },
  onLoad() {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight
    })
  },
  onLogin() {
    if (this.data.username.length == 0) {
      interaction.toast("账号不能为空!")
      return false
    }
    if (this.data.password.length == 0) {
      interaction.toast("密码不能为空!")
      return false
    }
    interaction.showLoading("登录中...")
    api.login({
      phone: this.data.username,
      password: this.data.password
    }).then(res => {
      // interaction.hideLoading()
      interaction.toast("登录成功!")
      wx.setStorageSync("userInfo", res.profile)
      wx.setStorageSync("access_token", res.token)
      getApp().globalData.HAS_LOGIN  = false
      let pages = getCurrentPages();
      if (pages.length >= 1) {
        let index = pages.length - 2
        pages[index].onShow()
        pages[index].onLoad()
      }
      setTimeout(() => {
        wx.navigateBack()
      }, 1000)
    })
  },
  onSetLogin() {
    this.setData({
      username: "13022721298",
      password: "password"
    })
  }
})
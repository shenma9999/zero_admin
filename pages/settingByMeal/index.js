import api from "../../common/api";
import interaction from "../../common/interaction";
import broadcast from "../../common/broadcast"
Page({
  data: {
    status: 0,
    navHeight: 0,
    active: 0,
    checked: false,
    showType: false,
    selectedAction: {
      name: "储值",
      category: 1
    },
    actions: [
      {
        name: "储值",
        category: 1
      },
      {
        name: "计次",
        category: 2
      },
      {
        name: "自定义充值",
        category: 4
      },
    ],
    meal: {},
    mealName: "",
    showMealName: false, // 是否显示设置套餐名称
    showRecharge: false, // 显示充值
    showGive: false, // 显示赠送
    showCountPrice: false, // 计次价格
    showCount: false, // 次数
    rechargeMoney: "",
    showMin: false,
    minMoney: "",
    giveMoney: "",
    countMoney: "",
    count: "",
    uid: ""
  },
  onDel() {
    let that = this
    wx.showModal({
      content: "是否删除当前套餐?",
      confirmText: "确定",
      cancelText: "取消",
      success: function (e) {
        if (e.confirm) {
          interaction.showLoading("删除中...")
          api.deleteTopupPlansByUid(that.data.uid).then(() => {
            interaction.toast("删除成功!")
            setTimeout(() => {
              broadcast.fire("onFreshClock") // 刷新价格
              wx.navigateBack()
            }, 1500)
          }, err => {
            interaction.toast(err.err_msg)
          })
        } else {
          // 用户点了取消
        }
      },
      fail(err) {
      }
    })
  },
  onLoad(options) {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      uid: options.uid || ""
    })
    // 查询详情
    if (options.uid) {
      interaction.showLoading("加载中...")
      api.getTopupPlansByUid(options.uid).then(res => {
        if (res.category == 1) {
          res.amount = parseFloat(res.amount)
          res.give_amount = parseFloat(res.give_amount)
        }
        if (res.category == 4) {
          res.min_amount = res.extra_json.min_amount
          this.setData({
            minMoney: res.min_amount
          })
        }
        this.setData({
          meal: res, 
          checked: res.status == 1
        })
        if (res.category == 2) {
          this.setData({
            selectedAction: this.data.actions[1]
          })
        } else if (res.category == 1) {
          this.setData({
            selectedAction: this.data.actions[0]
          })
        } else {
          this.setData({
            selectedAction: this.data.actions[2]
          })
        }
        interaction.hideLoading()
      }, err => {
        interaction.hideLoading()
      })
    }
  },
  onSave() {
    interaction.showLoading("处理中...")
    if (this.data.uid) {
      let meal = this.data.meal
      meal.status = this.data.checked ? 1 : 2
      meal.category = this.data.selectedAction.category
      meal.uid = this.data.uid
      if (meal.category == 4) {
        meal.min_amount = this.data.minMoney
        meal.max_amount = 999999999
      }
      api.setTopupPlans(meal).then(res => {
        interaction.toast("套餐应用成功!")
        setTimeout(() => {
          broadcast.fire("onFreshClock") // 刷新价格
          wx.navigateBack();
        }, 1500)
      }, err => {
        interaction.hideLoading()
      })
    } else {
      if (!this.data.mealName || !this.data.meal.name) {
        interaction.toast("请输入套餐名称")
        return false
      }
      if (this.data.selectedAction.category == 1) {
        if (this.data.rechargeMoney <= 0) {
          interaction.toast("请输入充值金额")
          return false
        }
      } else if (this.data.selectedAction.category == 2) {
        if (this.data.countMoney <= 0 || this.data.meal.price <= 0) {
          interaction.toast("请输入计次价格")
          return false
        }
        if (this.data.count <= 0) {
          interaction.toast("请输入次数")
          return false
        }
      } else { // 自定义充值
        if (this.data.minMoney <= 0) {
          interaction.toast("请输入最小充值金额")
          return false
        }
      }
      let meal = this.data.meal
      meal.status = this.data.checked ? 1 : 2
      meal.category = this.data.selectedAction.category
      if (meal.category == 1) {
        meal.amount = meal.price
      } else if (meal.category == 2) {
        meal.give_amount = 0
      } else {
        meal.min_amount = this.data.minMoney
        meal.max_amount = 999999999
      }
      api.createTopupPlans(meal).then(res => {
        interaction.toast("套餐应用成功!")
        setTimeout(() => {
          broadcast.fire("onFreshClock") // 刷新价格
          wx.navigateBack();
        }, 1500)
      }, err => {
        interaction.hideLoading()
      })
    }
  },
  onChange(e) {
    this.setData({
      checked: e.detail
    })
  },
  onShowType() {
    this.setData({
      showType: true
    })
  },
  // 显示套餐名称设置
  onShowMealName() {
    this.setData({
      showMealName: true
    })
  },
  onSetMeal(event) {
    let str = "meal.name"
    this.setData({
      showMealName: false,
      [str]: this.data.mealName || ""
    })
  },
  // 设置充值
  onSetRecharge(event) {
    let str = "meal.price"
    let str1 = "meal.amount"
    this.setData({
      showRecharge: false,
      [str1]: this.data.rechargeMoney || 0,
      [str]: this.data.rechargeMoney || 0
    })
  },
  // 设置赠送
  onSetGive() {
    let str = "meal.give_amount"
    this.setData({
      showGive: false,
      [str]: this.data.giveMoney || 0
    })
  },
  // 设置次数价格
  onSetCountPrice() {
    let str = "meal.price"
    this.setData({
      showCountPrice: false,
      [str]: this.data.countMoney || 0
    })
  },
  // 设置次数
  onSetCount() {
    let str = "meal.amount"
    this.setData({
      showCount: false,
      [str]: this.data.count || 0
    })
  },
  onSetMin() {
    let str = "meal.min_amount"
    this.setData({
      showMin: false,
      [str]: this.data.minMoney || 0
    })
  },
  // 显示充值弹窗
  onShowRecharge() {
    this.setData({
      showRecharge: true
    })
  },
  // 显示赠送金额弹窗
  onShowGive() {
    this.setData({
      showGive: true
    })
  },
  // 显示次数价格
  onShowPrice() {
    this.setData({
      showCountPrice: true
    })
  },
  // 显示最小金额
  onShowMin() {
    this.setData({
      showMin: true
    })
  },
  // 显示次数
  onShowCount() {
    this.setData({
      showCount: true
    })
  },
  onCloseMeal() {
    this.setData({
      showMealName: false
    })
  },
  onConfirmSelect(event) {
    let detail = event.detail
    if (detail.category ==  this.data.selectedAction.category) {
      return false
    } else {
      this.setData({
        meal: {
          name: "",
          price: 0,
          amount: 0,
          count: 0
        },
        rechargeMoney: 0,
        countMoney: 0,
        count: 0
      })
    }
    this.setData({
      showType: false,
      selectedAction: detail
    })
  },
  onCancelSelect() {
    this.setData({
      showMin: false,
      showType: false,
      showCount: false,
      showRecharge: false,
      showCountPrice: false,
      showGive: false
    })
  }
})

import api from "../../common/api.js"
import interaction from "../../common/interaction"
import utils from '../../common/utils'
Page({
  data: {
    status: 0,
    navHeight: 0,
    vipDate: "",
    user: {}
  },
  onLoad(options) {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight
    })
    interaction.showLoading("加载中...")
    api.getMemberDetail(options.uid).then(res => {
      this.setData({
        user: res,
        vipDate: res.member_card.zero_member_expired_at && utils.dateFormat("yyyy/MM/dd", res.member_card.zero_member_expired_at)
      })
      interaction.hideLoading()
    }).catch(ex => {
      interaction.hideLoading()
    })
  }
})

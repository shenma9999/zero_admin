import interaction from "../../common/interaction"
import api from "../../common/api"
import broadcast from "../../common/broadcast"
Page({
  data: {
    status: 0,
    navHeight: 0,
    active: 0,
    times: 0,
    list: [],
    clock: {price: ""},
    count: {price: ""}
  },
  onLoad() {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight
    })
    broadcast.on("onFreshCount", () => {
      this.getList()
    })
    broadcast.on("onFreshClock", () => {
      this.getList()
    })
    this.getList()
  },
  // 获取套餐列表
  getList() {
    interaction.showLoading("加载中...")
      // 获取计次列表
    api.getBillingMethods().then(res => {
      res && res.length > 0 && res.forEach((element, i) => {
        if (element.billing_method == 1) { // 计时
          this.setData({
            clock: element
          })
        }
        if (element.billing_method == 2) { // 计次
          this.setData({
            count: element
          })
        }
      });
      interaction.hideLoading()
    }, err => {
      interaction.hideLoading()
    })
    // 套餐列表
    api.getTopupPlans().then(res => {
      res && res.length > 0 && res.forEach(item => {
        if (item.category == 1) {
          item.amount = parseFloat(item.amount)
          item.give_amount = parseFloat(item.give_amount)
        }
      })
      this.setData({
        list: res || []
      })
    })
  },
  toSettingMeal(event) {
    let uid = event.currentTarget.dataset.uid
    wx.navigateTo({
      url: "/pages/settingByMeal/index?uid=" + (uid||"")
    })
  }
})

import interaction from "../../common/interaction"
import api from "../../common/api"
import broadcast from "../../common/broadcast"
Page({
  data: {
    status: 0,
    navHeight: 0,
    active: 0,
    clock: {},
    price: 0,
    maxPrice: 0,
    showSetPrice: false,
    showSetMaxPrice: false,
    checked: false,
    uid: ""
  },
  onLoad(options) {
    const app = getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      uid: options.uid
    })
    this.getPaymodeClock()
  },
  getPaymodeClock() {
    interaction.showLoading("加载中...")
    api.getBillingMethodsByUid(this.data.uid).then(res => {
      let price = parseFloat(res.price)
      let maxPrice = 0
      if (res.extra_json.max_amount) {
        maxPrice = parseFloat(res.extra_json.max_amount)
      }
      this.setData({
        clock: res,
        price: price || 1,
        maxPrice: maxPrice
      })
      interaction.hideLoading()
    }, err => {
      interaction.hideLoading()
    })
  },
  save() {
    interaction.showLoading("应用中...")
    if (this.data.maxPrice && parseFloat(this.data.maxPrice) > 0) {
      if (this.data.price && parseFloat(this.data.price) > 0) {
        if (parseFloat(this.data.maxPrice) < parseFloat(this.data.price)) {
          interaction.toast("封顶价必须大于等于计时价!")
          return false
        }
      }
    }
    api.setBillingMethodsByUid({
      uid: this.data.uid,
      price: this.data.price || this.data.clock.price,
      max_amount: this.data.maxPrice || (this.data.clock.extra_json && this.data.extra_json.max_amount) || 0,
      status: this.data.clock.status ? 1 : 2
    }).then(res => {
      interaction.hideLoading()
      interaction.toast("计时价格应用成功!")
      let str = "clock.price"
      let str1 = "clock.maxPrice"
      this.setData({
        [str]: this.data.price,
        [str1]: this.data.maxPrice
      })
      setTimeout(() => {
        broadcast.fire("onFreshClock") // 刷新价格
        wx.navigateBack()
      }, 2000)
    }, err => {
      interaction.hideLoading()
    })
  },
  onClosePrice() {
    this.setData({
      showSetPrice: false
    })
  },
  onCloseMaxPrice() {
    this.setData({
      showSetMaxPrice: false
    })
  },
  onSetPrice() {
    let price = ""
    try {
      price = parseFloat(this.data.price)
    } catch (ex) {
      price = 1
    }
    this.setData({
      price: price
    })
    let str = "clock.price"
    this.setData({
      [str]: this.data.price
    })
  },
  onSetMaxPrice() {
    let price = ""
    try {
      price = parseFloat(this.data.maxPrice)
      if (price <= 0 || isNaN(price)) {
        price = 1
      }
    } catch (ex) {
      price = 1
    }
    this.setData({
      maxPrice: price
    })
    let str = "clock.extra_json.max_amount"
    this.setData({
      [str]: this.data.maxPrice
    })
  },
  onShowSetPrice() {
    this.setData({
      showSetPrice: true
    })
  },
  onShowSetMaxPrice() {
    this.setData({
      showSetMaxPrice: true
    })
  },
  onInputMoney(event) {
    // let price = ""
    // try {
    //   price = parseInt(this.data.price)
    // } catch (ex) {
    //   price = 1
    // }
    // this.setData({
    //   price: price
    // })
  },
  onInputMaxMoney(event) {
    // let price = ""
    // try {
    //   price = parseInt(this.data.maxPrice)
    //   if (price <= 0 || isNaN(price)) {
    //     price = 1
    //   }
    // } catch (ex) {
    //   price = 1
    // }
    // this.setData({
    //   maxPrice: price
    // })
  },
  onChange(e) {
    let str = "clock.status"
    this.setData({
      [str]: !!e.detail
    })
  }
})
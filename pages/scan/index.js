import interaction from "../../common/interaction"
import api from "../../common/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  onTabItemTap(item) {
  　//具体操作
    console.log(item)
    // 允许从相机和相册扫码
    wx.scanCode({
      success (res) {
        console.log("扫码后==>", res)
        if (res.errMsg == 'scanCode:ok') {
          let results = res.result.split(":")
          if (results[2] == 'IN') {
            api.playEnterScan({
              order_uid: results[0],
              payment_method: (results[1] == 1 || results[1] == 3) ? 3 : 4  // 1 表示余额支付 0 表示次卡支付
            }).then(result => { // 验证通过
              if (result.extra_json.started_at || result.extra_json.ended_at) { // 开始打球通过
                wx.setStorageSync("order_info", result)
                wx.navigateTo({
                  url: "/pages/scanResult/index?order_uid=" + results[0] + "&category=" + result.category
                })
              }
            }, err => { // 验证失败
              setTimeout(() => {
                wx.switchTab({
                  url: "/pages/main/index"
                })
              }, 1200)
              interaction.toast(err.err_msg)
            })
          } else if (results[2] == 'OUT') { // 扫码出场
            api.playExitScan({
              order_uid: results[0]
            }).then(result => { // 验证通过
              if (result.extra_json.started_at || result.extra_json.ended_at) { // 开始打球通过
                wx.setStorageSync("order_info", result)
                wx.navigateTo({
                  url: "/pages/scanResult/index?order_uid=" + results[0] + "&category=" + result.category
                })
              }
            }, err => { // 验证失败
              setTimeout(() => {
                wx.switchTab({
                  url: "/pages/main/index"
                })
              }, 1200)
              interaction.toast(err.err_msg)
            })
          }
        } else {
          interaction.toast("请扫描进场或退场码!")
        }
        // wx.navigateTo({
        //   url: '/pages/scanResult/index'
        // })
      },
      fail (res) {
        wx.switchTab({
          url: "/pages/main/index"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
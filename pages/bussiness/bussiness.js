// pages/bussiness/bussiness.js
import sys from "../../common/sys.js"
import api from "../../common/api.js"
import interaction from "../../common/interaction.js"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    preview: false,
    src: null,
    from: "noref",
    far: true,
    init: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      from: options.from
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.checkNearbyArena()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  capture: function () {
    sys.capture().then( res => {
      this.setData({
        preview: true,
        src: res
      })
    })
  },

  reCapture: function () {
    this.setData({
      preview: false,
      src: null
    })
  },

  uploadFace: function () {
    api.uploadFace(this.data.src).then(res => {
      if (this.data.from == "tabbar") {
        // 统计埋点：人脸上传成功
        let auth_player = wx.getStorageSync('auth_player')
        wx.reportAnalytics('upload_face_success', {
          player_created_at: auth_player.created_at,
          player_uid: auth_player.uid,
        });
      }

      interaction.toast('上传成功')
      wx.navigateBack({
        delta: 1
      })
    }).catch(err => {
      // console.log('uploadFace setting err', err)
    })
  },

  checkNearbyArena: function () {
    return new Promise((resolve, reject) => {
      sys.getLocation().then(res => {
        api.getDistance().then(distance => {
          api.getNearByArenas(res.longitude, res.latitude, distance, 1).then(res => {
            if (res.results.length > 0) {
              this.setData({ far: false, init: true })
            } else {
              this.setData({ far: true, init: true })
            }
          })
        })
      })
    })
  },

})
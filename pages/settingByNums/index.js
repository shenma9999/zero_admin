import interaction from "../../common/interaction"
import api from "../../common/api"
import broadcast from "../../common/broadcast"
Page({
  data: {
    status: 0,
    navHeight: 0,
    active: 0,
    count: {},
    price: "",
    showSetPrice: false,
    checked: false,
    uid: ""
  },
  onLoad(options) {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      uid: options.uid
    })
    this.getPaymodeCount()
  },
  getPaymodeCount() {
    interaction.showLoading("加载中...")
    api.getBillingMethodsByUid(this.data.uid).then(res => {
      this.setData({
        count: res
      })
      interaction.hideLoading()
    }, err => {
      interaction.hideLoading()
    })
  },
  save() {
    interaction.showLoading("应用中...")
    api.setBillingMethodsByUid({
      uid: this.data.uid,
      price: this.data.price || this.data.count.price,
      status: this.data.count.status ? 1 : 2
    }).then(res => {
      interaction.hideLoading()
      interaction.toast("计次价格应用成功!")
      let str = "count.price"
      this.setData({
        [str]: this.data.price
      })
      setTimeout(() => {
        broadcast.fire("onFreshCount") // 刷新价格
        wx.navigateBack()
      }, 2000)
    }, err => {
      interaction.hideLoading()
    })
  },
  onClosePrice() {
    this.setData({
      showSetPrice: false
    })
  },
  onSetPrice() {
    let price = ""
    try {
      price = parseFloat(this.data.price)
      if (price <= 0 || isNaN(price)) {
        price = 1
      }
    } catch (ex) {
      price = 1
    }
    this.setData({
      price: price
    })
    let str = "count.price"
    this.setData({
      [str]: this.data.price
    })
  },
  onShowSetPrice() {
    this.setData({
      showSetPrice: true
    })
  },
  onInputMoney(event) {
    // let price = ""
    // try {
    //   price = parseInt(this.data.price)
    //   if (price <= 0 || isNaN(price)) {
    //     price = 1
    //   }
    // } catch (ex) {
    //   price = 1
    // }
    // this.setData({
    //   price: price
    // })
  },
  onChange(e) {
    let str = "count.status"
    this.setData({
      [str]: !!e.detail
    })
  }
})

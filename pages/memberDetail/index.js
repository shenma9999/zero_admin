import api from "../../common/api.js"
import interaction from "../../common/interaction"
import utils from '../../common/utils'
Page({
  data: {
    status: 0,
    navHeight: 0,
    user: {},
    uid: "",
    regDate: "",
    vipDate: "",
    checked: false
  },
  onLoad(options) {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      uid: options.uid
    })
    interaction.showLoading("加载中...")
    api.getMemberDetail(options.uid).then(res => {
      res.member_card.balance = parseFloat(res.member_card.balance).toFixed(2)
      this.setData({
        user: res,
        vipDate: res.member_card.zero_member_expired_at && utils.dateFormat("yyyy/MM/dd", res.member_card.zero_member_expired_at),
        regDate: utils.dateFormat("yyyy/MM/dd", res.member.created_at)
      })
      interaction.hideLoading()
    }).catch(ex => {
      interaction.hideLoading()
    })
  },
  onChange(e) {
    this.setData({
      checked: e.detail
    })
  }
})

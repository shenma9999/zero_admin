//index.js
import api from "../../common/api.js"
import auth from "../../common/auth.js"
import utils from "../../common/utils.js"
import interaction from "../../common/interaction"

Page({
  data: {
    active: 0,
    status: 0,
    navHeight: 0,
    background: "#e9e9e9",
    showSelect: false,
    minDate: new Date(new Date().getFullYear() - 1, new Date().getMonth(), 1).getTime(),
    maxDate: new Date().getTime(),
    currentTime: new Date().getTime(),
    // areaList: areaList,
    pickerConfig: {
      limitStartTime: "",
      limitEndTime: "",
      endDate: true,
      column: "second",
      pickerType: 'date',
      dateLimit: false
    },
    selectedAction: {
      name: "今日",
      flag: "today"
    },
    actions: [{
        name: "昨日",
        flag: "yestoday"
      },
      {
        name: "今日",
        flag: "today"
      },
      {
        name: "本周",
        flag: "week"
      },
      {
        name: "本月",
        flag: "month"
      },
      {
        name: "本年",
        flag: "year"
      },
      {
        name: "自定义",
        flag: "date"
      }
    ],
    time_start: "",
    time_end: "",
    dateStart: "",
    dateEnd: "",
    page: 1,
    per_page: 20,
    order_tab: 1, // 1打球订单 2视频、视频会员订单，默认1
    showCalendar: false, // 显示日期选择
    orderList: [],
    dateFlag: "", // 点击的时间标识  start 是开始时间  end 是结束时间
    money: 0,
    allMoney: 0
  },
  onLoad: function() {
    this.setNavSize()
    this.setStyle()
      // 默认今天数据查询
    this.setData({
      time_start: new Date(utils.dateFormat('yyyy/MM/dd 00:00:00', new Date().getTime())).getTime(),
      time_end: new Date(utils.dateFormat('yyyy/MM/dd 23:59:59', new Date().getTime())).getTime(),
      dateStart: utils.dateFormat('yyyy-MM-dd', new Date().getTime()),
      dateEnd: utils.dateFormat('yyyy-MM-dd', new Date().getTime()) // 后两天
    })
  },
  onShow() {
    this.setData({
      money: 0,
      orderList: []
    })
    this.getOrderList()
  },
  onHide: function() {
    this.setData({ onPlayVideoUid: null })
  },
  // 通过获取系统信息计算导航栏高度
  setNavSize: function() {
    var sysinfo = wx.getSystemInfoSync()
    var statusHeight = sysinfo.statusBarHeight
    var isiOS = sysinfo.system.indexOf("iOS") > -1
    var navHeight;
    if (!isiOS) {
      navHeight = 48;
    } else {
      navHeight = 44;
    }
    this.setData({
      status: statusHeight,
      navHeight: navHeight
    })
  },
  setStyle: function() {
    var containerStyle,
      textStyle,
      iconStyle;
    containerStyle = ["background:" + this.background].join(";");
    textStyle = [
      "color:" + (this.data.color || "rgba(0, 0, 0, 0.9)"),
      "font-size:" + (this.data.fontSize || 18) + "px"
    ].join(";");
    iconStyle = [
      "width: " + (this.data.iconWidth || 34) + "px",
      "height: " + (this.data.iconHeight || 34) + "px"
    ].join(";");
    this.setData({
      containerStyle: containerStyle,
      textStyle: textStyle,
      iconStyle: iconStyle
    })
  },
  getCurrentWeek() {
    // 起止日期数组 
    var startStop = new Array();
    // 获取当前时间 
    var currentDate = new Date();
    // 返回date是一周中的某一天 
    var week = currentDate.getDay();
    // 返回date是一个月中的某一天 
    var month = currentDate.getDate();

    // 一天的毫秒数 
    var millisecond = 1000 * 60 * 60 * 24;
    // 减去的天数 
    var minusDay = week != 0 ? week - 1 : 6;
    // alert(minusDay); 
    // 本周 周一 
    var monday = new Date(currentDate.getTime() - (minusDay * millisecond));
    // 添加本周时间 
    startStop.push(utils.dateFormat("yyyy/MM/dd", monday)); //本周起始时间 
    // 本周 周日 
    var sunday = monday.setDate(monday.getDate() + 6);
    // 添加本周最后一天时间 
    startStop.push(utils.dateFormat("yyyy/MM/dd", sunday)); //本周终止时间 
    //返回 
    return startStop;
  },
  //  获取指定的日期
  getSpedate(flag) {
    let date = new Date()
    let returnValue = {}
    let month = date.getMonth() + 1; // 表示当前月份的变量
    let year = date.getFullYear(); //通过时间来获取年份
    switch (flag) {
      case 'yestoday': // 昨天
        date.setDate(date.getDate() - 1);
        let yestoday = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
        returnValue = {
          start: utils.dateFormat('yyyy/MM/dd 00:00:00', new Date(yestoday)),
          end: utils.dateFormat('yyyy/MM/dd 23:59:59', new Date(yestoday))
        }
        break;
      case 'week': // 本周
        let arr = this.getCurrentWeek()
        returnValue = {
          start: utils.dateFormat('yyyy/MM/dd 00:00:00', arr[0]),
          end: utils.dateFormat('yyyy/MM/dd 23:59:59', arr[1])
        }
        break;
      case 'today':
        returnValue = {
          start: utils.dateFormat('yyyy/MM/dd 00:00:00', date),
          end: utils.dateFormat('yyyy/MM/dd 23:59:59', date)
        }
        break;
      case 'month':
        var newDate = new Date(year, month - 1, 32); // 根据月份设置日期
        var day = 32 - newDate.getDate();
        debugger
        returnValue = {
          start: (year + '/' + month + '/1'),
          end: (year + '/' + month + '/' + day)
        }
        break;
      case 'year':
        returnValue = {
          start: year + '/1/1',
          end: year + '/12/31'
        }
        break;
    }
    return returnValue
  },
  onSelectTab(e) {
    let index = e.currentTarget.dataset.active
    if (this.data.active == index) {
      return false
    }
    this.setData({
      money: 0,
      page: 1,
      orderList: [],
      active: index,
      order_tab: index + 1
    })
    this.reset()
    this.getOrderList()
  },
  reset() {
    this.setData({
      selectedAction: {
        name: "今日",
        flag: "today"
      },
      time_start: new Date(utils.dateFormat('yyyy/MM/dd 00:00:00', new Date())).getTime(),
      time_end: new Date(utils.dateFormat('yyyy/MM/dd 23:59:59', new Date())).getTime(),
      orderList: [],
      page: 1
    })
  },
  getOrderList(flag) {
    interaction.showLoading("加载中...")
    if (flag) {
      this.setData({
        page: this.data.page + 1
      })
    }
    console.log("列表请求日期开始时间===>", utils.dateFormat('yyyy/MM/dd hh:mm:ss', this.data.time_start))
    console.log("列表请求日期开始时间===>", utils.dateFormat('yyyy/MM/dd hh:mm:ss', this.data.time_end))
    api.getOrderList({
      time_start: this.data.time_start,
      time_end: this.data.time_end,
      page: this.data.page,
      per_page: this.data.per_page,
      order_tab: this.data.order_tab, // 1打球订单 2视频、视频会员订单，默认1
    }).then(res => {
      interaction.hideLoading()
      res.orders && res.orders.length > 0 && res.orders.forEach(element => {
        if (element.payment_method == 4) {
          element.total_amount = parseInt(element.total_amount)
        }
        element.payment_method_cn = utils.getPaymethodStr(element.payment_method)
      });
      this.setData({
        money: (parseFloat(res.income) + parseFloat(this.data.money)).toFixed(2),
        orderList: this.data.orderList.concat(res.orders)
      })
    }, err => {
      interaction.hideLoading()
    })
  },
  onShowColumn() {
    this.setData({
      showSelect: true
    })
  },
  onShowDate(event) {
    this.setData({
      selectedAction: {
        name: "自定义",
        flag: "date"
      }
    })
    if (this.data.selectedAction.flag == 'date') {
      let timeFlag = event.currentTarget.dataset.item
      let currentTime = 0
      if (timeFlag == 'start') {
        currentTime = new Date(this.data.dateStart && this.data.dateStart.replace(/-/g, '/')).getTime()
      } else {
        currentTime = new Date(this.data.dateEnd && this.data.dateEnd.replace(/-/g, '/')).getTime()
      }
      this.setData({
        dateFlag: timeFlag, // start 或者 是end
        showCalendar: true,
        currentTime: currentTime
      })
    } else {
      interaction.toast("选择自定义后才能选择日期范围!")
    }
  },
  onCloseCalendar() {
    this.setData({
      showCalendar: false
    })
  },
  // 选择日期类别
  onConfrimSelect(event) {
    const selectItem = event.detail;
    if (selectItem.name == this.data.selectedAction.name) {
      return false
    }
    console.log("固定日期===>", this.getSpedate(selectItem.flag))
    if (selectItem.flag != "date") {
      this.setData({
        page: 1,
        money: 0,
        orderList: [],
        selectedAction: selectItem,
        showSelect: false,
        time_start: new Date(this.getSpedate(selectItem.flag).start).getTime(),
        time_end: new Date(this.getSpedate(selectItem.flag).end).getTime()
      })
      this.getOrderList()
    } else { // 自定义日期
      this.setData({
        selectedAction: selectItem,
        showSelect: false,
      })
    }
  },
  onCancelSelect() {
    this.setData({
      showSelect: false
    })
  },
  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {
    this.setData({
      page: 1,
      money: 0,
      show: false,
      orderList: []
    })
    this.getOrderList()
    wx.stopPullDownRefresh()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {
    this.getOrderList(true)
  },
  pickerHide: function() {
    this.setData({
      showCalendar: false
    });
  },
  setPickerTime(val) {
    console.log(val);
    let data = val.detail;
    let date = data.date // 选择的日期
    if (this.data.dateFlag == 'start') {
      if (new Date(utils.dateFormat("yyyy/MM/dd", date)).getTime > this.data.time_end) {
        interaction.toast("前面的日期不能大于后面的日期！")
      } else {
        this.setData({
          page: 1,
          money: 0,
          orderList: [],
          dateStart: utils.dateFormat("yyyy-MM-dd", date),
          time_start: new Date(utils.dateFormat("yyyy/MM/dd", date)).getTime()
        });
        this.getOrderList()
      }
    } else if (this.data.dateFlag == 'end') {
      if (new Date(utils.dateFormat("yyyy/MM/dd", date)).getTime < this.data.time_start) {
        interaction.toast("后面的日期不能小于前面的日期！")
      } else {
        this.setData({
          page: 1,
          money: 0,
          orderList: [],
          dateEnd: utils.dateFormat("yyyy-MM-dd", date),
          time_end: new Date(utils.dateFormat("yyyy/MM/dd", date)).getTime()
        });
        this.getOrderList()
      }
    }
  }
})
import api from "../../common/api.js"
import auth from "../../common/auth.js"
import utils from "../../common/utils.js"
import interaction from "../../common/interaction"
Page({
  data: {
    orderList: [],
    uid: "",
    active: 0,
    page: 1,
    show: false,
    order_tab: 1,
    status: 0,
    navHeight: 0,
    nickname: ""
  },
  onLoad(options) {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      uid: options.uid,
      nickname: decodeURIComponent(options.nickname)
    })
    this.getOrderList()
  },
  onChange: function(e) {
    let index = e.detail.index
    this.setData({
      active: index,
      page: 1,
      orderList: [],
      show: false,
      order_tab: index + 1
    })
    this.getOrderList()
    console.log(index)
  },
  getOrderList(flag) {
    interaction.showLoading("加载中...")
    if (flag) {
      this.setData({
        page: this.data.page + 1
      })
    }
    console.log("列表请求日期开始时间===>", utils.dateFormat('yyyy/MM/dd hh:mm:ss', this.data.time_start))
    console.log("列表请求日期开始时间===>", utils.dateFormat('yyyy/MM/dd hh:mm:ss', this.data.time_end))
    api.getOrderList({
      order_tab: this.data.order_tab,
      page: this.data.page,
      per_page: this.data.per_page || 20,
      user_uid: this.data.uid
    }).then(res => {
      interaction.hideLoading()
      res.orders && res.orders.length > 0 && res.orders.forEach(element => {
        if (element.payment_method == 4) {
          element.total_amount = parseInt(element.total_amount)
        }
        let hour = new Date(element.extra_json.started_at || element.created_at).getHours()
        let str = ""
        if (hour >= 5 && hour < 8) {
          str = ("早上");
         } else if (hour >= 8 && hour < 11) {
          str = ("上午");
         } else if (hour >= 11 && hour < 13) {
          str = ("中午");
         } else if (hour >= 13 && hour < 18) {
          str = ("下午");
         } else {
          str = ("晚上");
         }
         if (element.extra_json && element.extra_json.started_at) {
            element.extra_json.started_at = utils.dateFormat('yyyy/MM/dd ' + str + ' hh:mm', (element.extra_json.started_at || element.created_at))
         } else {
           element.extra_json = {}
           element.extra_json.started_at = utils.dateFormat('yyyy/MM/dd ' + str + ' hh:mm', (element.extra_json.started_at || element.created_at))
         }
         element.payment_method_cn = utils.getPaymethodStr(element.payment_method)
      });
      this.setData({
        money: res.income,
        orderList: this.data.orderList.concat(res.orders)
      })
    })
  },
  onPullDownRefresh: function () {
    this.setData({
      page: 1,
      show: false,
      orderList: []
    })
    this.getOrderList()
    wx.stopPullDownRefresh()
  },
   /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
  },
  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    this.getOrderList(true)
  },
})

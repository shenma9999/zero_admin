import interaction from "../../common/interaction"
import api from "../../common/api"
// import { areaList } from '@vant/area-data';
import utils from "../../common/utils";
Page({
  data: {
    host: getApp().globalData.OBSHost,
    status: 0,
    navHeight: 0,
    fileList: [],
    showUpload: false,
    showSetName: false,
    showArea: false, // 地区
    courtName: "",
    showAddress: false,
    address: "",
    showTime: false,
    time: "",
    url: "",
    // areaList: areaList,
    pickerConfig: {
      limitStartTime: "",
      limitEndTime: "",
      endDate: true,
      column: "second",
      pickerType: 'time',
      abc: 123,
      dateLimit: false
    },
    courtInfo: {}
  },
  onLoad() {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight
    })
    this.getCourtDetail()
  },
  // 球馆详情
  getCourtDetail() {
    api.getCourtDetail().then(res => {
      let date = utils.dateFormat("yyyy-MM-dd", new Date())
      let pickerConfig = {}
      if (res.business_hours) {
        let strs = res.business_hours.split("-")
        pickerConfig = {
          initStartTime: date + " " + strs[0],
          initEndTime: date + " " + strs[1],
          limitStartTime: date + " " + strs[0],
          limitEndTime: date + " " + strs[1],
          endDate: true,
          pickerType: "time",
          column: "second",
          dateLimit: false
        }
      }
      this.setData({
        courtInfo: res,
        pickerConfig: pickerConfig,
        // fileList: [{
        //   url: this.data.host + '/' + res.brand.logo_path
        // }]
        url: this.data.host + '/' + res.brand.logo_path
      })
    })
  },
  onShareAppMessage() {
  },
  onShowSetName() {
    this.setData({
      showSetName: true
    })
  },
  onCloseName() {
    this.setData({
      showSetName: false,
      showAddress: false
    })
  },
  // 确认设置
  onSetName() {
  },
  onShowUpload() {
    let that = this
    wx.chooseImage({
      count: 1,
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths
        that.setData({
          fileList:[{
            url: tempFilePaths[0]
          }]
        })
      }
    })
  },
  onShowAddress() {
    this.setData({
      showAddress: true
    })
  },
  // onShowArea() {
  //   this.setData({
  //     showArea: true
  //   })
  // },
  // onCancelArea() {
  //   this.setData({
  //     showArea: false
  //   })
  // },
  // onConfrimArea(event) {
  //   debugger
  //   const area = event.detail
  //   area.values[0]
  // },
  onShowTime() {
    this.setData({
      showTime: true
    })
  },
  onSelectCourt() {
    wx.navigateTo({
      url: "/pages/selectCourt/index"
    })
  },
  pickerHide: function() {
    this.setData({
      showTime: false
    });
  },
  onLogout() {
    interaction.modal("退出登录", "确定要退出登录吗?", "确定", "", "取消").then(res => {
      interaction.showLoading("退出中...")
      api.logout().then(res => {
        interaction.toast("退出成功!")
        setTimeout(() => {
          wx.navigateTo({
            url: "/pages/login/login"
          })
        }, 1000)
      })
    })
  },
  pickerHide: function() {
    this.setData({
      showTime: false
    });
  },
  setPickerTime(val) {
    console.log(val);
    debugger
    let data = val.detail;
    this.setData({
      time: data.startTime + ' - ' + data.endTime
    })
    if (this.data.courtInfo.business_hours == (data.startTime + '-' + data.endTime)) {
      return false
    }
    api.setCourtDetail({
      business_hours: data.startTime + '-' + data.endTime
    }).then(res => {
      interaction.toast("营业时间修改成功")
      this.getCourtDetail()
    }, err => {
      interaction.toast(err.err_msg)
    })
  }
})

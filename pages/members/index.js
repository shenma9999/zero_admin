//index.js
import api from "../../common/api.js"
import interaction from "../../common/interaction"
Page({
  data: {
    status: 0,
    navHeight: 0,
    page: 1,
    searchKey: "",
    show: false,
    members: []
  },
  onLoad() {
    const app =  getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight
    })
    this.getMembers()
  },
  getMembers(flag) {
    if (flag && !this.data.show) {
      this.setData({
        page: this.data.page + 1
      })
    }
    if (this.data.show) {
      return false
    }
    interaction.showLoading("加载中...")
    api.getMemberList({
      page: this.data.page,
      per_page: 25,
      phone: this.data.searchKey
    }).then(res => {
      if (res && res.length > 0) {
        this.setData({
          members: this.data.members.concat(res)
        })
      } else {
        this.setData({
          show: true
        })
      }
      interaction.hideLoading()
    }).catch(ex => {
      this.setData({
        show: true
      })
      interaction.hideLoading()
    })
  },
  onPullDownRefresh: function () {
    this.setData({
      page: 1,
      show: false,
      members: []
    })
    this.getMembers()
    wx.stopPullDownRefresh()
  },
  onReachBottom() {
    this.getMembers(true)
  },
  onSearch() {
    if (this.data.searchKey) {
      this.setData({
        page: 1,
        members: [],
        show: false
      })
      this.getMembers()
    }
  },
  onClear() {
    this.setData({
      searchKey: "",
      members: []
    })
    this.getMembers()
  },
  onChange(e) {
    this.setData({
      searchKey: e.detail,
    });
    if (e.detail == "") {
      this.onClear()
    }
  },
})

import interaction from "../../common/interaction"
import api from "../../common/api"
import utils from "../../common/utils";
Page({
  data: {
    status: 0,
    navHeight: 0,
    order_uid: "",
    order_info: {},
    intervalId: 0,
    remark: "",
    showRemark: false
  },
  onLoad(options) {
    const app = getApp();
    this.setData({
      status: app.globalData.statusBarHeight,
      navHeight: app.globalData.navHeight,
      order_uid: options.order_uid
    })
    if (!options.order_uid) {
      wx.showModal({
        content: "没有拿到Order_uid"
      })
    }
    this.getOrder()
      // 查询订单状态 status  10 未支付  20 已支付 30 已取消   40 已关闭  50 已退款
    let order = this.data.order_info
    order.statusStr = utils.getStatusStr(order.status)
      // 1:其他 2:微信支付 3:余额支付 4:次卡支付
    order.payment_method_cn = utils.getPaymethodStr(order.payment_method)
      // if (order.status == 10) {
      //   interaction.showLoading("等待支付")
      // }
    let inOrOut = options.inOrOut // 进场或者出场
    this.setData({
      order_info: order
    })
    if (options.payType != 1) { // 计时进场
      // interaction.showLoading("等待客户支付...")
      api.getOrderDetail(this.data.order_uid).then(res => {
        if (res.status == 20) {
          interaction.toast("客户支付成功!")
          order = res
          order.statusStr = utils.getStatusStr(order.status)
            // 1:其他 2:微信支付 3:余额支付 4:次卡支付
          order.payment_method_cn = utils.getPaymethodStr(order.payment_method)
          this.setData({
            order_info: order
          })
        }
      }, err => {
        interaction.toast(err.err_msg)
        interaction.hideLoading()
      })
    }
  },
  onHide() {
    clearInterval(this.data.intervalId)
  },
  toHome() {
    wx.switchTab({
      url: "/pages/main/index"
    })
  },
  onShowRemark() {
    this.setData({
      showRemark: true
    })
  },
  onSetRemark() {
    if (!this.data.remark) {
      interaction.toast("备注不能为空!")
      return false
    }
    interaction.showLoading("处理中...")
    api.setOrderRemark(this.data.order_info.uid, this.data.remark).then(res => {
      interaction.hideLoading()
      interaction.toast("备注添加成功!")
      let str = "order_info.remark"
      this.setData({
        [str]: this.data.remark
      })
    }, err => {
      interaction.hideLoading()
    })
  },
  onCloseRemark() {
    this.setData({
      showRemark: false
    })
  },
  getOrder() {
    this.setData({
      order_info: wx.getStorageSync("order_info")
    })
  }
})
// components/tabbar/tabbar.js
import broadcast from '../../common/broadcast.js'
import interaction from '../../common/interaction.js'
import auth from '../../common/auth.js'
import utils from '../../common/utils.js'
import sys from '../../common/sys.js'
import api from '../../common/api.js'
import cache from '../../common/cache.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    pageIndex: {
      type: Number,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    tabbarList: [{
        "buttonId": 0,
        "text": "订单",
        "icon": "/assets/tabs/bill.gray.png",
        "selectedIcon": "/assets/tabs/bill.select.png",
        "path": "/pages/main/index"
      },
      {
        "buttonId": 1,
        "text": "包场",
        "icon": "/assets/tabs/calendar.gray.png",
        "selectedIcon": "/assets/tabs/calendar.select.png",
        "path": "/pages/reserve/index"
      },
      {
        "buttonId": -1,
        "icon": "/assets/scan.png",
        "selectedIcon": "/assets/scan_pressed.png",
        "text": "验票",
      },
      {
        "buttonId": 2,
        "text": "会员",
        "icon": "/assets/tabs/vip.gray.png",
        "selectedIcon": "/assets/tabs/vip.select.png",
        "path": "/pages/members/index"
      },
      {
        "buttonId": 3,
        "text": "配置",
        "icon": "/assets/tabs/setting.gray.png",
        "selectedIcon": "/assets/tabs/setting.select.png",
        "path": "/pages/setting/index"
      }
    ],
    isLogged: false,
    timing: '00:00:00',
    interval: null,
    showStartTip: false,
    showEndTip: false,
    bottomHeight: getApp().globalData.bottomHeight
  },

  ready: function() {},

  pageLifetimes: {
    // 组件所在页面的生命周期函数
    show: function() {},
    hide: function() {},
    resize: function() {}
  },

  /**
   * 组件的方法列表
   */
  methods: {
    selectPage(e) {
      const { id, path } = e.currentTarget.dataset
      if (id == -1) {
        this.onScan()
        return false
      }
      if (this.data.pageIndex === id) {
        return
      }
      wx.switchTab({
        url: path
      })
    },
    onScan() {
      wx.scanCode({
        success(res) {
          console.log("扫码后==>", res)
          interaction.showLoading("处理中...")
          if (res.errMsg == 'scanCode:ok') {
            if (res) {
              res = JSON.parse(res.result)
            }
            // {"status":"success","data":{"gid":"22732514493861888","title":"\u8ba1\u6b21\u6253\u7403","payment_method":4,"out_trade_no":"22732514498056192","paid_at":1621935053808,"total_amount":"1.00","paid_amount":"1.00","status":20,"category":3,"extra_json":null,"remark":"","created_at":1621935053805,"updated_at":1621935053809,"uid":"VDvq0N9Rd1"}}
            let now = new Date().getTime()
            let twominutes = 1000 * 60 * 2
            let diff = now - res.updated_at
            debugger
            if (diff > twominutes) {
              interaction.toast("二维码超时!请用户刷新")
              return false
            }
            api.scanQrCode(res.uid).then(result => {
              interaction.hideLoading()
              setTimeout(() => {
                if (res.scene_no == 1) { // 进场
                  wx.setStorageSync("order_info", result)
                  wx.navigateTo({
                    url: "/pages/scanResult/index?inOrOut=in&payType=" + result[1] + "&order_uid=" + result.uid + "&category=" + result.category
                  })
                } else { // 出场
                  wx.setStorageSync("order_info", result)
                  wx.navigateTo({
                    url: "/pages/scanResult/index?inOrOut=out&order_uid=" + result.uid + "&category=" + result.category
                  })
                }
              }, 1800)
            }, err => {
              interaction.hideLoading()
              interaction.toast(err.err_msg)
            })
          } else {
            interaction.toast("请扫描进场或退场码!")
          }
          // wx.navigateTo({
          //   url: '/pages/scanResult/index'
          // })
        },
        fail(res) {
          wx.switchTab({
            url: "/pages/main/index"
          })
        }
      })
    },
  }
})
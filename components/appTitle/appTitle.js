Component({
  properties: {
    title: {
      type: String,
      value: '标题'
    },
    showBack: {
      type: Boolean,
      value: true
    }
  },
  data: {},
  ready() {},
  methods: {
    backClickHandler() {
      wx.navigateBack()
    }
  }
})

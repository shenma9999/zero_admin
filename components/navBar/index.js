// components/tabbar/tabbar.js
import broadcast from "../../common/broadcast.js"
import interaction from "../../common/interaction.js"
import auth from "../../common/auth.js"
import utils from "../../common/utils.js"
import sys from "../../common/sys.js"
import api from "../../common/api.js"
import cache from "../../common/cache.js"

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    background: {
      type: String,
      value: "rgba(255, 255, 255, 1)"
    },
    backDeepth: { // 返回深度 默认上一个页面
      type: Number,
      value: 1
    },
    color: {
      type: String,
      value: "rgba(0, 0, 0, 1)"
    },
    titleText: {
      type: String,
      value: ""
    },
    titleImg: {
      type: String,
      value: ""
    },
    backIcon: {
      type: Boolean,
      value: true
    },
    homeIcon: {
      type: Boolean,
      value: false
    },
    dropName: {
      type: String,
      value: ""
    },
    fontSize: {
      type: Number,
      value: 16
    },
    iconHeight: {
      type: Number,
      value: 19
    },
    iconWidth: {
      type: Number,
      value: 58
    },
    borders: {
      type: Boolean,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    status: 0,
    navHeight: 0
  },

  ready: function () {
    auth.isLogged().then(res => {
      wx.getStorage({
        key: 'showStartTip',
      }).then(res => {
        if (res.data) {
          this.setData({showStartTip: true})
        }else{
          this.setData({showStartTip: false})
        }
      }).catch(err => {
        this.setData({showStartTip: true})
      })
      this.setData({
        ongoing: wx.getStorageSync('onPlayBasketBall') ? true : false,
        isLogged: true
      })
    }).catch(err => {
      this.setData({
        ongoing: false,
        isLogged: false
      })
    })

    broadcast.on('showed_tip', (status) => {
      this.setData({ showStartTip: false })
      wx.setStorage({
        data: false,
        key: 'showStartTip',
      })
    })

    broadcast.on('start_stop_workout', (status) => {
      if (status) {
        wx.setStorageSync('startWorkoutAt', Date.parse(new Date()))
        this.createInterval()
      } else {
        clearInterval(this.data.interval)
        wx.removeStorageSync('startWorkoutAt')
        this.setData({showEndTip: false})
      }
      wx.setStorageSync('onPlayBasketBall', status)
      this.setData({
        ongoing: status,
        timing: "00:00:00"
      })
    })
    
    broadcast.on('loginSuccess', (status) => {
      this.setData({
        ongoing: wx.getStorageSync('onPlayBasketBall') ? true : false,
        isLogged: true
      })
    })
  },

  pageLifetimes: {
    // 组件所在页面的生命周期函数
    show: function () { 
      this.setNavSize()
      this.setStyle()
      console.log(this.titleText)
    },
    hide: function () { },
    resize: function () { },
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 通过获取系统信息计算导航栏高度
    setNavSize: function() {
      var sysinfo = wx.getSystemInfoSync()
      var statusHeight = sysinfo.statusBarHeight
      var isiOS = sysinfo.system.indexOf("iOS") > -1
      var navHeight;
      if (!isiOS) {
        navHeight = 48;
      } else {
        navHeight = 44;
      }
      this.setData({
        status: statusHeight,
        navHeight: navHeight
      })
    },
    setStyle: function() {
      var containerStyle,
        textStyle,
        iconStyle;
      containerStyle = ["background:" + this.data.background].join(";");
      textStyle = [
        "color:" + (this.data.color || "rgba(0, 0, 0, 0.9)"),
        "font-size:" + (this.data.fontSize || 18) + "px"
      ].join(";");
      iconStyle = [
        "width: " + (this.data.iconWidth || 34) + "px",
        "height: " + (this.data.iconHeight || 34) + "px"
      ].join(";");
      this.setData({
        containerStyle: containerStyle,
        textStyle: textStyle,
        iconStyle: iconStyle
      })
    },
    // 返回事件
    back() {
      wx.navigateBack({
        delta: this.data.backDeepth || 1
      });
      this.triggerEvent("back", { back: 1 });
    },
    home() {
      this.triggerEvent("home", {});
    }
  }
})

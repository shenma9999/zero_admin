// components/returnButton/returnButton.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    backToMenu: {
      type: Boolean,
      value: false
    },

    defaultStyle: {
      type: Boolean,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    statusBarHeight: getApp().globalData.statusBarHeight
  },

  /**
   * 组件的方法列表
   */
  methods: {
    lastPage: function (e) {
      if (this.data.backToMenu) {
        // 统计埋点：分享页面点击home按钮
        let auth_player = wx.getStorageSync('auth_player')
        let auth_player_uid = auth_player ? auth_player.uid : ''
        wx.reportAnalytics('click_landing_page_home_btn', {
          player_uid: auth_player_uid,
          player_created_at: auth_player ? auth_player.created_at : 0,
        });
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
      else {
        wx.navigateBack({
          complete: (res) => {},
        })
      }
    }
  }
})

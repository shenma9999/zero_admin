// components/tap/tap.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    defaultImage: {
      type: String,
      value: ""
    },

    pressedImage: {
      type: String,
      value: ""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isTap: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    tapStart: function (e) {
      this.setData({isTap: true})
    },

    tapEnd: function (e) {
      this.setData({isTap: false})
    }
  }
})

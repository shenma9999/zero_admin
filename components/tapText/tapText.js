// components/tapText/tapText.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    opacity: 1.0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    tapStart: function (e) {
      this.setData({opacity: 0.6})
    },

    tapEnd: function (e) {
      this.setData({opacity: 1.0})
    }
  }
})

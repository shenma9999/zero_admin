//计算到当前时间到差值
var differenceTime = function (timestamp) {
  timestamp = new Date(timestamp).getTime() / 1000
  const curData = Date.parse(new Date()) / 1000
  const dif = curData - timestamp
  // console.log(curData, timestamp,dif)
  if (dif > 86400) {
    return parseInt(dif / 86400) + '天前'
  }

  if (dif > 3600) {
    return parseInt(dif / 3600) + '小时前'
  }

  if (dif > 60) {
    return parseInt(dif / 60) + '分钟前'
  }

  if (dif < 0) {
    return '0秒前'
  }

  return parseInt(dif) + '秒前'
}

// 秒转分秒
var secondToTime = function (duration) {
  let minutes = parseInt(duration / 60)
  let seconds = parseInt(duration % 60)
  let minutesStr = minutes > 9 ? String(minutes) : '0' + String(minutes)
  let secondsStr = seconds > 9 ? String(seconds) : '0' + String(seconds)
  return minutesStr + ':' + secondsStr
}
// 获取订单状态值
const getStatusStr = (status) => {
  let statusStr = ""
  if (status == 10) {
    statusStr = '未支付'
  } else if (status == 20) {
    statusStr = '已支付'
  } else if (status == 30) {
    statusStr = '已取消'
  } else if (status == 40) {
    statusStr = '已关闭'
  } else if (status == 50) {
    statusStr = '已退款'
  }
  return statusStr
}
// 获取支付方式=值
const getPaymethodStr = (payment_method) => {
  let payment_method_cn = ""
  if (payment_method == 1) {
    payment_method_cn = '其他'
  } else if (payment_method == 2) {
    payment_method_cn = '微信支付'
  } else if (payment_method == 3) {
    payment_method_cn = '余额支付'
  } else if (payment_method == 4) {
    payment_method_cn = '次卡支付'
  }
  return payment_method_cn
}
// 获取订单类型
const getOrderTypeStr = (category) => {
  let order_cn = ""
  if (category == 1) {
    order_cn = '统一支付'
  } else if (category == 2) {
    order_cn = '计时打球'
  } else if (category == 3) {
    order_cn = '计次打球'
  } else if (category == 4) {
    order_cn = '视频会员'
  } else if (category == 5) {
    order_cn = '单个视频'
  } else if (category == 6) {
    order_cn = '商品'
  } else if (category == 7) {
    order_cn = '储值卡支付'
  } else if (category == 8) {
    order_cn = '次卡支付'
  }
  return order_cn
}
// 秒转时分秒
var secondToFullTime = function (duration) {
  var hours = Math.floor(duration / 3600)
  var minutes = Math.floor((duration / 60) % 60)
  var seconds = Math.floor(duration % 60)
  let hoursStr = hours > 9 ? String(hours) : '0' + String(hours)
  let minutesStr = minutes > 9 ? String(minutes) : '0' + String(minutes)
  let secondsStr = seconds > 9 ? String(seconds) : '0' + String(seconds)
  return hoursStr + ':' + minutesStr + ':' + secondsStr
}

//时间戳转换成时间
var timestamp2Date = function (timestamp) {
  // ps, 必须是数字类型，不能是字符串, +运算符把字符串转化为数字，更兼容
  timestamp = timestamp * 1000
  const dateObj = new Date(+timestamp)
  const year = dateObj.getFullYear() // 获取年，
  const month = dateObj.getMonth() + 1 // 获取月，必须要加1，因为月份是从0开始计算的
  const day = dateObj.getDate() // 获取日，记得区分getDay()方法是获取星期几的。
  const week = dateObj.getDay()
  const hours = dateObj.getHours() // 获取时, pad函数用来补0
  const minutes = dateObj.getMinutes() // 获取分
  const seconds = dateObj.getSeconds() // 获取秒
  return {
    year,
    month,
    day,
    week,
    hours,
    minutes,
    seconds
  }
}
const dateFormat = (format, time) => {
  let dt = new Date()
  if (time) {
    if (typeof time !== 'number') {
      time = time.toString().replace(/-/g, '/')
    }
    dt = new Date(time)
  }
  var date = {
    'M+': dt.getMonth() + 1,
    'd+': dt.getDate(),
    'h+': dt.getHours(),
    'm+': dt.getMinutes(),
    's+': dt.getSeconds(),
    'q+': Math.floor((dt.getMonth() + 3) / 3),
    'S+': dt.getMilliseconds()
  }
  if (/(y+)/i.test(format)) {
    format = format.replace(RegExp.$1, (dt.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (var k in date) {
    if (new RegExp('(' + k + ')').test(format)) {
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? date[k] : ('00' + date[k]).substr(('' + date[k]).length)
      )
    }
  }
  return format
}
//格式化时间戳
var formatTime = function (timestamp) {
  var date = new Date(timestamp * 1000)
  var Y = date.getFullYear() + '-'
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-'
  var D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' '
  var h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':'
  var m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':'
  var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  return Y + M + D + h + m + s
}

//星期对应的文字
var convert2cnWeek = function (week) {
  let weeTex = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
  return weeTex[week]
}

var convert2EnMonth = function (month) {
  //英语月份表
  let engMonth = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  return engMonth[month]
}

var checkPhone = function (phone) {
  return new Promise((resolve, reject) => {
    if (!/^1[356789]\d{9}$/.test(phone)) {
      reject()
    } else {
      resolve(phone)
    }
  })
}

var arenaStatus = function (timeStr, status) {
  let startEnd = timeStr.split('-')
  let start = startEnd[0].split(':')
  let end = startEnd[1].split(':')
  let now = new Date()
  let hours = now.getHours()
  let minutes = now.getMinutes()
  let cur = hours * 60 + minutes
  let open = Number(start[0]) * 60 + Number(start[1]) <= cur && cur < Number(end[0]) * 60 + Number(end[1])

  if (open) {
    return status
  }

  return 5
}

function compare(property) {
  return function (a, b) {
    var value1 = a[property]
    var value2 = b[property]
    return value1 - value2
  }
}

var sort = function (arr, property) {
  arr.sort(compare(property))
}

function arenaRecommend() {
  return function (a, b) {
    var congestion1 = a['congestion'] / 5
    var congestion2 = b['congestion'] / 5
    if (a['congestion'] == 5) {
      congestion1 = 1000
    }
    if (b['congestion'] == 5) {
      congestion2 = 1000
    }
    var distance1 = a['distance'] / 20
    var distance2 = b['distance'] / 20
    var recommend1 = 10 - a['recommend']
    var recommend2 = 10 - b['recommend']
    if (congestion1 == 1) {
      congestion1 = 1000
    }

    return congestion1 + distance1 + recommend1 - congestion2 - distance2 - recommend2
  }
}

var arenaRecommendSort = function (arr) {
  arr.sort(arenaRecommend())
}
function rad(d) {
  return (d * Math.PI) / 180.0
}
// 根据经纬度计算距离，参数分别为第一点的纬度，经度；第二点的纬度，经度
var getDistance = function (lat1, lng1, lat2, lng2) {
  // console.log(lat1, lng1, lat2, lng2)
  var radLat1 = rad(lat1)
  var radLat2 = rad(lat2)
  var a = radLat1 - radLat2
  var b = rad(lng1) - rad(lng2)
  var s =
    2 *
    Math.asin(
      Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2))
    )
  s = s * 6378.137 // EARTH_RADIUS;
  s = Math.round(s * 10000) / 10000 //输出为公里

  var distance = s
  var distance_str = ''

  if (parseInt(distance) >= 1) {
    distance_str = distance.toFixed(1) + 'km'
  } else {
    distance_str = distance * 1000 + 'm'
  }


  //s=s.toFixed(4);

  // console.info('lyj 距离是', s);
  // console.info('lyj 距离是', distance_str);
  return s.toFixed(1)
}
var ymdToTimestamp = function (year, month, day = 1) {
  return Date.parse(new Date(year, month, day)) / 1000
}

var monthStartEnd = function (year, month) {
  let start = ymdToTimestamp(year, month, 1)
  var next_month = month + 1
  var next_month_year = year
  if (next_month > 11) {
    next_month = 0
    next_month_year += 1
  }
  let end = ymdToTimestamp(next_month_year, next_month, 1)
  return [start, end]
}
// 时间累加
const timePlus = (val) => {
  if (val < 60) {
    val = Math.floor(val)
    return '00' + ':00‘' + (val < 10 ? 0 + '' + val : val) + ''
  } else {
    var min_total = Math.floor(val / 60) // 分钟
    var sec = Math.floor(val % 60) // 余秒
    if (min_total < 60) {
      return '00:' + (min_total < 10 ? 0 + '' + min_total : min_total) + '’' + (sec < 10 ? 0 + '' + sec : sec) + '’’'
    } else {
      var hour_total = Math.floor(min_total / 60) // 小时数
      var min = Math.floor(min_total % 60) // 余分钟
      return (
        (hour_total < 10 ? 0 + '' + hour_total : hour_total) +
        ':' +
        (min < 10 ? 0 + '' + min : min) +
        '’' +
        (sec < 10 ? 0 + '' + sec : sec) +
        '’’'
      )
    }
  }
}
// 时间累加
const showTime = (val) => {
  if (val < 60) {
    val = Math.floor(val)
    // return "00" + ":00:" + (val < 10 ? (0 + "" + val) : val) + "";
    return val + '分钟'
  } else {
    var min_total = Math.floor(val / 60) // 分钟
    var sec = Math.floor(val % 60) // 余秒
    if (min_total < 60) {
      // return "00:" + (min_total < 10 ? (0 + "" + min_total) : min_total) + ":" + (sec < 10 ? (0 + "" + sec) : sec) + "";
      return (min_total < 10 ? 0 + '' + min_total : min_total) + '分钟'
    } else {
      var hour_total = Math.floor(min_total / 60) // 小时数
      var min = Math.floor(min_total % 60) // 余分钟
      return (hour_total < 10 ? 0 + '' + hour_total : hour_total) + '小时' + (min < 10 ? 0 + '' + min : min) + '分钟'
    }
  }
}
export default {
  timePlus,
  showTime,
  differenceTime,
  timestamp2Date,
  convert2EnMonth,
  secondToTime,
  secondToFullTime,
  checkPhone,
  convert2cnWeek,
  arenaStatus,
  sort,
  arenaRecommendSort,
  ymdToTimestamp,
  monthStartEnd,
  getDistance,
  formatTime,
  dateFormat,
  getStatusStr,
  getPaymethodStr,
  getOrderTypeStr
}

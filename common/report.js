
var _post = function (id, data) {
  let auth_player = wx.getStorageSync('auth_player')
  let auth_player_uid = auth_player ? auth_player.uid : ''
  var postData = data
  postData.player_created_at = auth_player ? auth_player.created_at : 0
  postData.player_uid = auth_player_uid
  wx.reportAnalytics(id, postData)
  console.log(postData)
}

var mineDataClick = function (btnName) {
  let data = {
    player_uid: '',
    player_created_at: 0,
    btn_name: btnName,
  }
  _post('mine_data_click', data)
}

var closeOpenRecommend = function (status) {
  let data = {
    player_uid: '',
    player_created_at: 0,
    status: status,
  }
  _post('close_open_recommend', data)
}


export default {
  mineDataClick,
  closeOpenRecommend
}
import constants from 'constants.js'
import interaction from 'interaction.js'

let apiHost = constants.API_HOST
let obsHost = constants.OBS_HOST

function request(url, method, data) {
  return new Promise((resolve, reject) => {
    const _header = {
      'content-type': 'application/json'
    }
    const AccessToken = wx.getStorageSync('access_token')
    if (AccessToken) {
      _header['Authorization'] = `Bearer ${AccessToken}`
    }
    console.log("接口请求==>", AccessToken, url, data)
    wx.request({
      url: apiHost + url,
      method: method,
      data: data,
      header: _header,
      success(res) {
        console.log("接口响应==>", res)
        if (res.statusCode == 200) {
          if (res.data.status == 'success') {
            resolve(res.data.data)
          } else {
            reject(res.data)
          }
        } else {
          // token 失效或者未登录
          if (res.data.err_code == 401000) {
            // 当前页面
            if (!getApp().globalData.HAS_LOGIN) {
              getApp().globalData.HAS_LOGIN  = true
              wx.navigateTo({
                url: '/pages/login/login'
              })
            }
            reject(res.data)
          } else {
            reject(res.data)
          }
        }
        // // console.log('request success', res)
        // if (res.statusCode == 201) {
        //   resolve('created')
        // } else if (res.statusCode == 204) {
        //   resolve('deleted')
        // } else if (res.data.status == 0) {
        //   resolve(res.data)
        // } else if (res.data.status == 1) {
        //   if (res.data.err_code == 401102) {
        //     interaction.toast('登录失效 请重新登录')
        //     auth.clear()
        //     sys.init()
        //     wx.reLaunch({
        //       url: '/pages/index/index'
        //     })
        //     // auth.logout().then(res => {
        //     //   wx.reLaunch({
        //     //     url: '/pages/index/index',
        //     //   })
        //     // }).catch(err => {
        //     //   wx.reLaunch({
        //     //     url: '/pages/index/index',
        //     //   })
        //     // })
        //   }
        //   reject(res.data)
        // } else {
        //   reject(res)
        // }
        // resolve(res)
      },
      fail(err) {
        // console.log('request fail', err)
        // reject(err)
        // processException(err)
        interaction.toast('网络请求失败，请检查您的网络状态')
      }
    })
  })
}

function processException(err) {
  console.log('Fatal Error =>', err.errMsg)
}

function processCommonUnsuccessRequest(err) {
  console.log('Fatal Error =>', err.errMsg)
}

var post = function (url, data) {
  return new Promise((resolve, reject) => {
    request(url, 'POST', data)
      .then((res) => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

var put = function (url, data) {
  return new Promise((resolve, reject) => {
    request(url, 'PUT', data)
      .then((res) => {
        resolve(res)
      }, err => {
        reject(err)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

var del = function (url, data) {
  return new Promise((resolve, reject) => {
    request(url, 'DELETE', data)
      .then((res) => {
        resolve(res)
      }, err => {
        reject(err)
      })
      .catch((err) => {
        reject(err)
      })
  })
}
var patch = function (url, data) {
  return new Promise((resolve, reject) => {
    request(url, 'PATCH', data)
      .then((res) => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}
var get = function (url, data) {
  return new Promise((resolve, reject) => {
    request(url, 'GET', data)
      .then((res) => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

var upload = function (url, fieldName, filePath) {
  return new Promise((resolve, reject) => {
    const AccessToken = wx.getStorageSync('access_token')
    wx.uploadFile({
      url: apiHost + url,
      filePath: filePath,
      header: {
        Authorization: 'Bearer ' + AccessToken
      },
      name: fieldName,
      // formData: {
      //   'userid': openId
      // },
      success: (res) => {
        var data = JSON.parse(res.data)
        if (res.statusCode == 200) {
          resolve(data.data)
        } else {
          // 失败
          reject(data)
        }
      },
      fail: (res) => {
        // 失败
        reject(res)
      }
    })
  })
}

export default {
  post,
  get,
  upload,
  del,
  put,
  patch
}

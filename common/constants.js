const constants = {
  API_HOST: __wxConfig.envVersion == 'release' ? 'https://dev.brand.zheshi.tech/api/v1' : 'https://dev.brand.zheshi.tech/api/v1',
  OBS_HOST: "https://obs.zheshi.tech",
  DEFAULT_AVATAR: "https://obs.zheshi.tech/wechat_static/default_avatar.png",
  DEFAULT_NICKNAME: "零号球场用户",
  NEARBY_DISTANCE: 1000,
}
module.exports = constants
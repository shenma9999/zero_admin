var _apply = function (scope = 'scope.userLocation', title = '是否授权', content = '请去设置授权，否则将导致功能无法使用') {
  return new Promise((resolve, reject) => {
    wx.getSetting({
      success: function (res) {
        var scopes = res.authSetting;
        if (!scopes[scope]) {
          // 申请去设置授权
          wx.showModal({
            title: title,
            content: content,
            success: function (e) {
              if (e.confirm) {
                wx.openSetting({
                  success: function (data) {
                    if (data.authSetting[scope] === true) {
                      // 获取授权成功
                      resolve()
                    } else {
                      //授权失败
                      reject()
                    }
                  },
                  fail: function (err) {
                    // 打开设置失败
                    reject()
                  }
                })
              } else {
                // 用户点了取消
                reject()
              }
            }
          })
        } else {
          resolve()
        }
      },
    })
  })
}

var location = function (title='需要获取您的地理位置', content='请去设置授权，否则将导致部分功能无法使用') {
  return _apply('scope.userLocation', title, content)
}

export default {
  location
}
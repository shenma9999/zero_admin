// 弹框
var toast = function (title, icon = "none", duration = 1500) {
  title += ""
  wx.showToast({
    title,
    duration: 1500,
    icon: icon
  });
}

// 加载中
var showLoading = function (title='加载中', mask=false) {
  wx.showLoading({
    title: title,
    mask: mask
  })
}

// 加载结束
var hideLoading = function () {
  wx.hideLoading()
}

//操作框
var actionSheet = function (list) {
  return new Promise((resolve, reject) => {
    wx.showActionSheet({
      itemList: list,
      success(res) {
        resolve(res.tapIndex)
      },
      fail(res) {
        // console.log(res.errMsg)
      }
    })
  })
}

var modal = function (title, content, confirmText = "确认", confirmColor ="#6236FF", cancelText="取消") {
  return new Promise((resolve, reject) => {
    wx.showModal({
      title: title,
      content: content,
      confirmText: confirmText,
      confirmColor: confirmColor,
      cancelText: cancelText,
      success: function (e) {
        if (e.confirm) {
          resolve()
        } else {
          // 用户点了取消
          reject()
        }
      }
    })
  })
}

var confirmModal = function (title, content, confirmText = "确认", confirmColor ="#6236FF") {
  return new Promise((resolve, reject) => {
    wx.showModal({
      title: title,
      content: content,
      confirmText: confirmText,
      confirmColor: confirmColor,
      showCancel: false,
      success: function (e) {
        if (e.confirm) {
          resolve()
        } else {
          // 用户点了取消
          reject()
        }
      }
    })
  })
}


export default {
  toast,
  actionSheet,
  modal,
  confirmModal,
  showLoading,
  hideLoading
}
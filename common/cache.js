import api from "api.js"
import constants from "constants.js"

var getArena = function(arenaUid) {
  return wx.getStorageSync('arena-' + arenaUid)
}

var storeArena = function (arena) {
  return wx.setStorageSync('arena-' + arena.uid, arena)
}

var getLike = function (videoUid) {
  return wx.getStorageSync('liked-video-' + videoUid)
}

var storeLike = function (videoUid, status) {
  return wx.setStorageSync('liked-video-' + videoUid, status)
}

var storeArenas = function (arenas) {
  for (var i = 0; i < arenas.length; i++) {
    storeArena(arenas[i])
  }
}

var getDistance = function () {
  return wx.getStorageSync('NEARBY_DISTANCE')
}

var storeDistance = function (distance) {
  return wx.setStorageSync('NEARBY_DISTANCE', distance)
}


export default {
  getArena,
  storeArena,
  storeArenas,
  storeLike,
  getLike,
  getDistance,
  storeDistance
}
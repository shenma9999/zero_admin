import auth from "auth.js"
import api from "api.js"

var getVideosWithLikeStatus = function (res) {
  return new Promise((resolve, reject) => {
    auth.isLogged().then(logged => {
      // 获取likes数据
      var videosUid = []
      for (var i = 0; i < res.length; i++) {
        videosUid.push(res[i].uid)
      }
      api.getLikedVideosUid(videosUid).then(vuids => {
        for (var i = 0; i < res.length; i++) {
          res[i].liked = vuids.indexOf(res[i].uid) != -1
        }
        resolve(res)
      }).catch(res => {
        resolve(res)
      })
    }).catch(nologged => {
      resolve(res)
    })
  })
}

export default {
  getVideosWithLikeStatus
}
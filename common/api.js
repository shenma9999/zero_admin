import requests from "requests.js"
import auth from "auth.js"
import interaction from "interaction.js"
import cache from "cache.js"

var getArenas = function (lng, lat, sortOp = 1) {
  return new Promise((resolve, reject) => {
    requests.get("/arenas-videos", {
      option: sortOp,
      lng: lng,
      lat: lat
    }).then(res => {
      cache.storeArenas(res.results)
      resolve(res)
    })
  });
}

var getAllArenas = function () {
  return new Promise((resolve, reject) => {
    requests.get("/arenas").then(res => {
      cache.storeArenas(res.results)
      resolve(res.results)
    })
  });
}

var getArena = function (arenaUid) {
  return new Promise((resolve, reject) => {
    let arena = cache.getArena(arenaUid)
    if (arena) {
      resolve(arena)
    } else {
      requests.get("/arenas/" + arenaUid).then(res => {
        cache.storeArena(res.results)
        resolve(res.results)
      })
    }
  });
}


var getWorkouts = function (perPage = 10, lastId = -1) {
  return new Promise((resolve, reject) => {
    var data = { 
      per_page: perPage,
      last_id: lastId
      }
    requests.get('/workouts', data).then(res => {
      resolve(res.results)
    })
  })
} 


// 个人信息
var getProfile = function (uid) {
  return new Promise((resolve, reject) => {
    requests.get('/players/' + uid + '/reports').then(res => {
      resolve(res.results)
    })
  })
}

// 人脸
var getFaces = function () {
  return new Promise((resolve, reject) => {
    requests.get('/players/' + auth.authPlayerUid() + '/faces').then(res => {
      resolve(res.results)
    })
  })
}

// 删除人脸
var deleteFace = function (id) {
  return new Promise((resolve, reject) => {
    requests.del('/players/' + auth.authPlayerUid() + '/faces', { id: id }).then(res => {
      resolve()
    })
  })
}

var getNearByArenas = function (lng, lat, distance, limit=-1) {
  return new Promise((resolve, reject) => {
    var data = {
      lng: lng, 
      lat: lat,
      distance: distance
    }
    if (limit > 0) {
      data.limit = limit
    }
    requests.get('/arenas', data).then(res => {
      resolve(res)
    })
  })
}


var delWorkout = function (workoutUid) {
  return new Promise((resolve, reject) => {
    requests.del('/workouts/' + workoutUid).then(res => {
      if (res == "deleted") {
        resolve()
      } else {
        reject()
      }
    }).catch(err => {
      reject(err)
    });
  })
}

var updatePhone = function (phone, code) {
  return new Promise((resolve, reject) => {
    requests.put('/players/' + auth.authPlayerUid(), { "phone": phone, "code": code }).then(res => {
      resolve(res)
    }).catch(err => {
      if (err.err_code == 401100) {
        interaction.toast("验证码不正确")
      } else if (data.err_code == 409100) {
        interaction.toast("该手机号已被其他账号绑定")
      } else {
        interaction.toast("手机号更新失败")
      }
      reject(err)
    });
  })
}

var uploadFace = function (path) {
  return new Promise((resolve, reject) => {
    requests.upload('/players/' + auth.authPlayerUid() + '/faces', 'face', path).then( res => {
      // console.log('uploadFace res', res)
      resolve()
    }).catch(err => {
      // console.log('uploadFace err', err)
      // 身份认证 (data.err_code == 401101) || (data.err_code == 401102)
      if (err.err_code == 400107) {
        // 人脸不合格 请正对摄像头并保证光线充足
        interaction.toast("人脸不合格 请正对摄像头并保证光线充足")
      } else if (err.err_code == 400109) {
        // 失败 每个用户最多允许上传6张照片
        interaction.toast("失败 每个用户最多允许上传6张照片")
      } else {
        // 上传失败或人脸不符合要求'
        interaction.toast("上传失败或人脸不符合要求")
      }
      reject()
    })
  })
}


var getDistance = function (useCache=true) {
  return new Promise((resolve, reject) => {
    if (useCache) {
      let cacheDistance = cache.getDistance()
      if (cacheDistance) {
        resolve(cacheDistance)
      } else {}
    }
    requests.get('/distance').then(res => {
      cache.storeDistance(res.results.distance)
      resolve(res.results.distance)
    }).catch(err => {
      resolve(constants.NEARBY_DISTANCE)
    })
  })
}


var getWorkoutsReport = function (playerUid, start_at, end_at) {
  return new Promise((resolve, reject) => {
    let data = {
      player_uid: playerUid,
      start_at: start_at,
      end_at: end_at
    }
    requests.get('/workouts-reports', data).then(res => {
      resolve(res.results)
    })
  })
}
// 用户登录
var login = function (data) {
  return new Promise((resolve, reject) => {
    requests.post(`/login`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 用户登出
var logout = function () {
  return new Promise((resolve, reject) => {
    requests.post(`/logout`).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 订单列表
var getOrderList = function (data) {
  return new Promise((resolve, reject) => {
    requests.get(`/orders`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 会员列表
var getMemberList = function (data) {
  return new Promise((resolve, reject) => {
    requests.get(`/members`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 会员详情
var getMemberDetail = function (user_uid) {
  return new Promise((resolve, reject) => {
    requests.get(`/members/${user_uid}`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 球馆详情
var getCourtDetail = function () {
  return new Promise((resolve, reject) => {
    requests.get(`/settings/info`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 球馆详情
var setCourtDetail = function (data) {
  return new Promise((resolve, reject) => {
    requests.post(`/settings/info`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 订单详情
var getOrderDetail = function (order_uid) {
  return new Promise((resolve, reject) => {
    requests.get(`/orders/${order_uid}`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}

//设置订单备注
var setOrderRemark = function (order_uid, remark) {
  return new Promise((resolve, reject) => {
    requests.put(`/orders/${order_uid}`, {remark: remark}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 查看计次套餐列表
var getBillingMethods = function () {
  return new Promise((resolve, reject) => {
    requests.get(`/billing-methods`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 查看计时计次详情
var getBillingMethodsByUid = function (uid) {
  return new Promise((resolve, reject) => {
    requests.get(`/billing-methods/${uid}`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 查看计时计次详情
var setBillingMethodsByUid = function (data) {
  return new Promise((resolve, reject) => {
    requests.put(`/billing-methods/${data.uid}`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 查看储值套餐列表
var getTopupPlans = function () {
  return new Promise((resolve, reject) => {
    requests.get(`/topup-plans`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 查看储值套餐详情
var getTopupPlansByUid = function (uid) {
  return new Promise((resolve, reject) => {
    requests.get(`/topup-plans/${uid}`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 删除套餐
var deleteTopupPlansByUid = function (uid) {
  return new Promise((resolve, reject) => {
    requests.del(`/topup-plans/${uid}`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 创建套餐
var createTopupPlans = function (data) {
  return new Promise((resolve, reject) => {
    requests.post(`/topup-plans`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 修改套餐
var setTopupPlans = function (data) {
  return new Promise((resolve, reject) => {
    requests.put(`/topup-plans/${data.uid}`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 进场扫码
var playEnterScan = function (data) {
  return new Promise((resolve, reject) => {
    requests.post(`/play/enter`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 离场扫码
var playExitScan = function (data) {
  return new Promise((resolve, reject) => {
    requests.post(`/play/exit`, data).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
// 进出场扫码确认
var scanQrCode = function (qrcode_uid) {
  return new Promise((resolve, reject) => {
    requests.put(`/qrcodes/${qrcode_uid}`, {}).then(res => {
      resolve(res)
    }, err => {reject(err)}).catch(ex => {
      reject(ex)
    })
  })
}
export default {
  scanQrCode,
  getTopupPlansByUid,
  deleteTopupPlansByUid,
  setTopupPlans,
  createTopupPlans,
  getBillingMethodsByUid,
  setBillingMethodsByUid,
  getBillingMethods,
  getTopupPlans,
  setOrderRemark,
  login,
  logout,
  getMemberList,
  getCourtDetail,
  getMemberDetail,
  getOrderList,
  getOrderDetail,
  playExitScan,
  playEnterScan,
  getArenas,
  getWorkouts,
  getProfile,
  getNearByArenas,
  uploadFace,
  getFaces,
  deleteFace,
  getArena,
  getAllArenas,
  updatePhone,
  delWorkout,
  getDistance,
  setCourtDetail,
  getWorkoutsReport,
}
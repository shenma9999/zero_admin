import interaction from 'interaction.js'
import wxp from 'promise.js'
import auth from 'auth.js'
import api from 'api.js'
import permission from 'permission.js'

var init = function () {
  auth.wxLogin()
  api.getAllArenas()
}

var getLocation = function () {
  return new Promise((resolve, reject) => {
    wxp.getLocation({ type: 'gcj02' }).then(res => {
      resolve(res)
    }).catch(err => {
      // console.log(err)
      if (err.errMsg == "getLocation:fail auth deny" || err.errMsg == "getLocation:fail authorize no response") {
        // 权限问题
        permission.location().then(res => {
          wxp.getLocation({ type: 'gcj02' }).then(res => {
            resolve(res)
          })
        }).catch(err => {
          reject()
        })
      } else {
        // 其他问题
        interaction.toast("定位失败，请检查微信定位权限及网络状态")
        reject()
      }
    })
  })
}

// 打开地图
var openMap = function (event) {
  wx.openLocation({
    latitude: event.currentTarget.dataset.lat,
    longitude: event.currentTarget.dataset.lng,
    name: event.currentTarget.dataset.name,
    address: event.currentTarget.dataset.address,
    scale: 15
  })
}

//打开摄像机
var onCamera = function () {
  return new Promise((resolve, reject) => {
    wx.createCameraContext().takePhoto({
      quality: 'high',
      success: resolve,
      fail: reject
    });
  })
}

//打开相册
var onAlbum = function () {
  return new Promise((resolve, reject) => {
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: resolve,
      fail: reject
    });
  })
}

//文件上传
var fileUpload = function (url, filePath, name) {
  return new Promise((resolve, reject) => {
    wx.uploadFile({
      url: root + url,
      filePath,
      name,
      header: {
        'X-Elephant-Token': wx.getStorageSync('token')
      },
      success: resolve,
      fail: reject
    })
  })
}

//下载文件
var download = function (url) {
  interaction.showLoading('下载中...')
  wx.downloadFile({
    url: url,
    success: (result) => {
      interaction.hideLoading()
      const tmp = result.tempFilePath
      wx.saveVideoToPhotosAlbum({
        filePath: tmp,
        success: (result) => {
          interaction.toast('已保存', 'success')
        },
        fail: () => {
          interaction.toast('下载失败')
        },
      });
    },
    fail: () => {
      interaction.hideLoading()
      interaction.toast('下载失败')
    },
  });
}

// 拍照返回路径
var capture = function () {
  return new Promise((resolve, reject) => {
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        resolve(res.tempImagePath)
      },
      error(e) {
        reject(e)
      },
    })
  })
}

var isWifi = function () {
  return new Promise((resolve, reject) => {
    wx.getNetworkType({
      success: function (res) {
        if (res.networkType == 'wifi') {
          resolve()
        } else {
          reject()
        }
      },
      fail: function (res) {
        reject()
      }
    })
  })
}


export default {
  openMap,
  download,
  getLocation,
  capture,
  init,
  isWifi
}
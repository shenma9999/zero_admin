import requests from 'requests.js'
import api from 'api.js'
import sys from 'sys.js'
import broadcast from 'broadcast.js'
import interaction from 'interaction.js'

var isLogged = function () {
  return new Promise((resolve, reject) => {
    try {
      wx.getStorageSync('userInfo') ? resolve(wx.getStorageSync('userInfo')) : reject()
    } catch (e) {
      reject()
    }
  })
}

var authPlayerUid = function () {
  let player = wx.getStorageSync('auth_player')
  if (player) {
    return player.uid
  } else {
    return null
  }
}

var authPlayer = function () {
  let player = wx.getStorageSync('auth_player')
  if (player) {
    return player
  } else {
    return null
  }
}

var authPlayerNickname = function () {
  let player = wx.getStorageSync('auth_player')
  if (player) {
    return player.nick_name
  } else {
    return null
  }
}

var clear = function () {
  wx.setStorageSync('auth', false)
  wx.removeStorageSync('auth_player')
  // wx.removeStorageSync('session_key')
  // wx.removeStorageSync('openid')
  wx.removeStorageSync('token')
  getApp().globalData.onPlayBasketBall = false
  getApp().globalData.isLogged = false
  // getApp().globalData.openid = null
  // getApp().globalData.session_key = null
}

var logout = function () {
  return new Promise((resolve, reject) => {
    requests.post('/auth/logout')
    try {
      clear()
      resolve()
    } catch (e) {
      reject()
    }
  })
}

var wxLogin = function () {
  wx.login({
    success: (res) => {
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      requests
        .get('/auth/wechat-mp-openids', {
          code: res.code
        })
        .then((res) => {
          // console.log(res)
          getApp().globalData.openid = res.results.openid
          getApp().globalData.session_key = res.results.session_key
          wx.setStorageSync('session_key', res.results.session_key)
          wx.setStorageSync('openid', res.results.openid)
        })
    },
    fail: (err) => {
      interaction.toast('初始化失败')
    }
  })
}

var login = function (userinfo) {
  return new Promise((resolve, reject) => {
    // interaction.showLoading("登录中...")
    let openid = wx.getStorageSync('openid')
    let session_key = wx.getStorageSync('session_key')
    if (openid == '' || session_key == '') {
      // interaction.hideLoading()
      sys.init()
      interaction.toast('初始化未完成或网络状态不佳 请稍后再试')
      return
    }

    requests
      .post('/auth/login', {
        open_id: openid,
        session_key: session_key,
        encrypt_data: userinfo.encryptedData,
        iv: userinfo.iv
      })
      .then((res) => {
        let auth_player = res.results
        wx.setStorageSync('auth_player', res.results)
        wx.setStorageSync('token', auth_player.token)
        wx.setStorageSync('uid', auth_player.uid)
        if (auth_player.phone) {
          // wx.setStorageSync('auth', true)
          // getApp().globalData.isLogged = true
          // broadcast.fire('loginSuccess')
          afterLogin(auth_player)
        }
        // 统计埋点：授权获取个人信息（getuserinfo）成功人数
        wx.reportAnalytics('getuserinfo_success', {
          player_created_at: auth_player.created_at,
          player_uid: auth_player.uid
        })
        // interaction.hideLoading()
        resolve(auth_player)
      })
      .catch((err) => {
        // interaction.hideLoading()
        sys.init()
        interaction.toast('登录请求失败 请稍后再试')
      })
  })
}

var bindPhone = function (userinfo) {
  return new Promise((resolve, reject) => {
    let session_key = wx.getStorageSync('session_key')
    var auth_player = wx.getStorageSync('auth_player')
    requests
      .post('/auth/phone', {
        session_key: session_key,
        encrypt_data: userinfo.encryptedData,
        iv: userinfo.iv,
        uid: auth_player.uid
      })
      .then((res) => {
        auth_player = res.results
        wx.setStorageSync('token', auth_player.token)
        wx.setStorageSync('uid', auth_player.uid)
        afterLogin(auth_player)
        // wx.setStorageSync('auth_player', auth_player)
        // wx.setStorageSync('auth', true)
        // getApp().globalData.isLogged = true
        // broadcast.fire('loginSuccess')
        resolve(res.results.phone)
      })
      .catch((err) => {
        if (err.err_code == 409100) {
          interaction.toast('该手机号已被绑定')
        } else {
          interaction.toast('手机号绑定失败')
        }
        reject(err)
      })
  })
}

function afterLogin(authPlayer) {
  wx.setStorageSync('auth_player', authPlayer)
  wx.setStorageSync('auth', true)
  getApp().globalData.isLogged = true
  broadcast.fire('loginSuccess')
}

var applayCode = function (phone) {
  return new Promise((resolve, reject) => {
    requests
      .get('/auth/phone-codes', { phone: phone })
      .then((res) => {
        resolve(res.results)
      })
      .catch((err) => {
        reject()
      })
  })
}

export default {
  login,
  logout,
  bindPhone,
  isLogged,
  authPlayerUid,
  authPlayerNickname,
  clear,
  wxLogin,
  applayCode,
  authPlayer
}

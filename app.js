//app.js
import requests from 'common/requests.js'
import api from 'common/api.js'
import constant from './common/constants.js'
import auth from 'common/auth.js'

App({
  globalData: {
    OBSHost: constant.OBS_HOST,
    HAS_LOGIN: false, // 是否去登录了
    navHeight: 0,
    isLogged: false, // 默认没有登陆
    uid: '', // player uid
    openid: null, // 用户openid
    session_key: null, // 用户session key，初始化小程序先像服务器获取
    onPlayBasketBall: false, // 是否正在场内，默认false。当底部tabbar组件点击start入场后改为true，用于初始化其他未曾点击页面内的tabbar组件
    statusBarHeight: wx.getSystemInfoSync()['statusBarHeight']
  },
  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
    auth.isLogged().then(res => {
    }, notLogin => {
      if (!this.globalData.HAS_LOGIN) {
        this.globalData.HAS_LOGIN = true
        wx.navigateTo({
          url: "/pages/login/login"
        })
      }
    })
    this.setNavSize()
  },
  // 通过获取系统信息计算导航栏高度
  setNavSize: function () {
    var sysinfo = wx.getSystemInfoSync()
    let screenHeight = sysinfo.screenHeight
    let bottom = sysinfo.safeArea && sysinfo.safeArea.bottom
    var isiOS = sysinfo.system.indexOf('iOS') > -1
    var isIphonex = screenHeight != bottom
    let navHeight = 0
    let bottomHeight = 0
    if (!isiOS) {
      navHeight = 48
    } else {
      navHeight = 44
    }
    if (isiOS && isIphonex) {
      bottomHeight = 30
    }
    this.globalData.navHeight = navHeight
    this.globalData.bottomHeight = bottomHeight
  }
})

// App({
//   onLaunch: function () {
//     // 展示本地存储能力
//     var logs = wx.getStorageSync('logs') || []
//     logs.unshift(Date.now())
//     wx.setStorageSync('logs', logs)

//     // 登录
//     wx.login({
//       success: res => {
//         // 发送 res.code 到后台换取 openId, sessionKey, unionId
//       }
//     })
//     // 获取用户信息
//     wx.getSetting({
//       success: res => {
//         if (res.authSetting['scope.userInfo']) {
//           // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
//           wx.getUserInfo({
//             success: res => {
//               // 可以将 res 发送给后台解码出 unionId
//               this.globalData.userInfo = res.userInfo

//               // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
//               // 所以此处加入 callback 以防止这种情况
//               if (this.userInfoReadyCallback) {
//                 this.userInfoReadyCallback(res)
//               }
//             }
//           })
//         }
//       }
//     })
//   },
//   globalData: {
//     userInfo: null,
//     buttonId: 0
//   }
// })
